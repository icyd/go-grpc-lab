package main

import (
	"context"
	"fmt"
	"log"
	"io"
	"google.golang.org/grpc"
	// "google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	// "google.golang.org/grpc/status"
	"github.com/icyd/go-grpc-lab/blog/blogpb"
	"time"
)
func main() {
	opts := grpc.WithInsecure()
	tls := false

	if tls {
		certFile := "ssl/ca.crt"
		creds, sslErr := credentials.NewClientTLSFromFile(certFile, "")
		if sslErr != nil {
			log.Fatalf("Failed to load certificates: %v\n", sslErr)
			return
		}

		opts = grpc.WithTransportCredentials(creds)
	}
	conn, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer conn.Close()

	c := blogpb.NewBlogServiceClient(conn)

	// blog := blogpb.Blog {
	// 	AuthorId: "TO DELETE",
	// 	Title: "My Blog",
	// 	Content: "Content of my deleted blog.",
	// }

	// blogID := "5fd4d702df25be2bb9495323"
	// retId := createBlog(c, &blog)
	// blogID := "5fd4e1eb5070795561488b64"

	// readBlog(c, blogID)
	// updateBlog(c, blogID, &blog)
	// deleteBlog(c, retId)
	listBlog(c)
}

func createBlog(c blogpb.BlogServiceClient, blog *blogpb.Blog) string {

	res, err := c.CreateBlog(context.Background(), &blogpb.CreateBlogRequest {
		Blog: blog,
	})
	if err != nil {
		log.Fatalf("Unexpeted error: %v", err)
	}
	fmt.Printf("Blog created: %v\n", res)
	return res.GetBlog().GetId()
}

func readBlog(c blogpb.BlogServiceClient, blogId string) {
	fmt.Println("Reading blog")

	_, err := c.ReadBlog(context.Background(), &blogpb.ReadBlogRequest {
		BlogId: "29292929292929",
	})
	if err != nil {
		fmt.Printf("Error happened while reading: %v", err)
	}

	readBlogReq := &blogpb.ReadBlogRequest{
		BlogId: blogId,
	}
	res, err := c.ReadBlog(context.Background(), readBlogReq)
	if err != nil {
		log.Fatalf("Error happened while reading: %v", err)
	}

	fmt.Printf("Blog read: %v\n", res.GetBlog())
}

func updateBlog(c blogpb.BlogServiceClient, blogId string, blog *blogpb.Blog) {
	fmt.Println("Update blog")

	blog.Id = blogId

	res, err := c.UpdateBlog(context.Background(), &blogpb.UpdateBlogRequest {
		Blog: blog,
	})
	if err != nil {
		fmt.Printf("Error while updating blog: %v\n", err)

	}
	fmt.Printf("Blog updated: %v\n", res.GetBlog())
}

func deleteBlog(c blogpb.BlogServiceClient, blogId string) {
	fmt.Println("Delete blog")

	res, err := c.DeleteBlog(context.Background(), &blogpb.DeleteBlogRequest {
		BlogId: blogId,
	})
	if err != nil {
		fmt.Printf("Error while deleting blog: %v\n", err)
	}
	fmt.Printf("Blog deleted: %v\n", res.GetBlogId())
}

func listBlog(c blogpb.BlogServiceClient) {
	fmt.Println("List blog")
	stream, err := c.ListBlog(context.Background(), &blogpb.ListBlogRequest{})
	if err != nil {
		fmt.Printf("Error while listing blogs: %v\n", err)
	}
	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatalf("Something happened: %v\n", err)
		}
		fmt.Println(res.GetBlog())
		time.Sleep(1000*time.Millisecond)
	}
}
