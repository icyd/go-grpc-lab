package main

import (
	"context"
	"fmt"
	"log"
	"io"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/codes"
	"github.com/icyd/go-grpc-lab/calc/calcpb"
	"sync"
	"time"
)
func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer conn.Close()

	c := calcpb.NewCalcServiceClient(conn)
	doUnary(c)
	doServerStreaming(c)
	doClientStreaming(c)
	doBiDiStreaming(c)
	doErrorUnary(c, 10)
	// doErrorUnary(c, 2)
	// doErrorUnary(c, 0)
	// doErrorUnary(c, -5)
}

func doUnary(c calcpb.CalcServiceClient) {
	fmt.Println("Starting unary RPC...")

	req := &calcpb.CalcRequest {
		Operators: &calcpb.Operators {
			V1: 5,
			V2: 10,
		},
	}
	res, err := c.Add(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling Greet RPC: %v", err)
	}
	log.Printf("Response from greet: %v", res.Result)
}

func doServerStreaming(c calcpb.CalcServiceClient) {
	fmt.Println("Starting server streaming RPC...")

	req := &calcpb.CalcPrimeRequest {
		Number: 12390392840,
	}

	stream, err := c.PrimeNumberDecomposition(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling PrimeNumberDecomposition RPC: %v", err)

	}

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatalf("Error while reading stream: %v", err)
		} else {
			log.Printf("Response from PrimeNumberDecomposition: %v", msg.GetResult())
		}
	}
}

func doClientStreaming(c calcpb.CalcServiceClient) {
	fmt.Println("Starting client streaming RPC...")

	numbers := []int64{3, 5, 9, 54, 23}

	stream, err := c.ComputeAverage(context.Background())
	if err != nil {
		log.Fatalf("Error while calling cliest stream RPC: %v", err)
	}

	for _, number := range numbers {
		fmt.Printf("Sending number: %v\n", number)
		stream.Send(&calcpb.ComputeAverageRequest {
			Number: number,
		})
		time.Sleep(1000 * time.Millisecond)
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error while receiving from ComputeAverage: %v", err)
	}

	fmt.Printf("ComputeAverage Response: %v\n", res.GetResult())

}

func doBiDiStreaming(c calcpb.CalcServiceClient) {
	fmt.Println("Starting bidi streaming RPC...")
	numbers := []int64{1, 5, 3, 6, 2, 20, 18}

	stream, err := c.FindMaximum(context.Background())
	if err != nil {
		log.Fatalf("Error while creating bidi stream: %v\n", err)
		return
	}

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		for _, number := range numbers {
			fmt.Printf("Sending data: %v\n", number)
			stream.Send(&calcpb.FindMaximumRequest {
				Number: number,
			})
			time.Sleep(1000 * time.Millisecond)
		}
		stream.CloseSend()
		wg.Done()
	}()

	go func() {
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				break
			} else if err != nil {
				log.Fatalf("Error while receiving data: %v\n", err)
				break
			}
			fmt.Printf("Maximum: %v\n", res.GetResult())
		}
		wg.Done()
	}()

	wg.Wait()
}

func doErrorUnary(c calcpb.CalcServiceClient, number int32) {
	fmt.Println("Starting bidi streaming RPC...")
	res, err := c.SquareRoot(context.Background(), &calcpb.SquareRootRequest {
		Number: number,
	})

	if err != nil {
		resErr, ok := status.FromError(err)
		if ok {
			fmt.Println(resErr.Message())
			fmt.Println(resErr.Code())
			if resErr.Code() == codes.InvalidArgument {
				fmt.Println("Negative number was sent")
			}
			return
		} else {
			log.Fatalf("Error while receiving from server: %v\n", resErr)
			return
		}
	}
	fmt.Printf("Square root of %v = %v\n", number, res.GetResult())
}
