package main

import (
	"context"
	"fmt"
	"log"
	"io"
	"math"
	"net"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"github.com/icyd/go-grpc-lab/calc/calcpb"
)

type server struct{
	calcpb.UnimplementedCalcServiceServer
}

func (*server) Add(ctx context.Context, req *calcpb.CalcRequest) (*calcpb.CalcResponse, error) {
	fmt.Printf("Greet function was invoked with %v\n", req)
	v1 := req.GetOperators().GetV1();
	v2 := req.GetOperators().GetV2();
	result := v1 + v2
	response := &calcpb.CalcResponse {
		Result: result,
	}
	return response, nil
}

func (*server) PrimeNumberDecomposition(req *calcpb.CalcPrimeRequest, stream calcpb.CalcService_PrimeNumberDecompositionServer) error {
	rest := int64(req.GetNumber())
	fmt.Printf("Calculating prime factors for: %v\n", rest)
	prime := int64(2)
	fmt.Printf("New number to check: %v\n", prime)
	for rest > 1 {
		if rest % prime == 0 {
			res := &calcpb.CalcPrimeResponse {
				Result: prime,
			}
			stream.Send(res)
			rest = rest / prime
		} else {
			prime++
			fmt.Printf("New number to check: %v\n", prime)

		}
	}

	return nil

}

func (*server) ComputeAverage(stream calcpb.CalcService_ComputeAverageServer) error {
	fmt.Println("ComputeAverage function was invoked")
	sum := int64(0)
	n := 0

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			return stream.SendAndClose(&calcpb.ComputeAverageResponse {
				Result: float64(sum) / float64(n),
			})

		} else if err != nil {
			log.Fatalf("Error while reading client stream: %v\n", err)
		}
		sum += msg.GetNumber()
		n++
	}

	return nil
}

func (*server) FindMaximum(stream calcpb.CalcService_FindMaximumServer) error {
	fmt.Println("FindMaximum function was invoked")
	max := int64(0)

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		} else if err != nil {
			log.Fatalf("Error while reading client stream: %v\n", err)
			return err
		}
		recvNum := req.GetNumber()
		if recvNum > max {
			max = recvNum
			sendErr := stream.Send(&calcpb.FindMaximumResponse {
				Result: max,
			})
			if sendErr != nil {
				log.Fatalf("Error while sending data to client: %v\n", sendErr)
				return sendErr
			}
		}
	}

}

func (*server) SquareRoot(ctx context.Context, req *calcpb.SquareRootRequest) (*calcpb.SquareRootResponse, error) {
	fmt.Println("SquareRoot function was invoked")
	number := req.GetNumber()

	if number < 0 {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("Received negative number: %v", number),
		)
	}

	return &calcpb.SquareRootResponse {
		Result: math.Sqrt(float64(number)),
	}, nil
}

func main() {
	bind := "0.0.0.0:50051"
	listener, err := net.Listen("tcp", bind)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	fmt.Println("Listening on:", bind)

	s := grpc.NewServer()
	calcpb.RegisterCalcServiceServer(s, &server{})
	reflection.Register(s)
	if err := s.Serve(listener); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
