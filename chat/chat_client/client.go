package main

import (
		"bufio"
		"context"
		"fmt"
		"google.golang.org/grpc"
		"github.com/icyd/go-grpc-lab/chat/chatpb"
		"io"
		"log"
		"os"
		"sync"
)

func main() {
		if len(os.Args) < 3 {
				log.Fatalln("Must give url as first argument and username as second")
		}

		ctx := context.Background()

		conn, err := grpc.Dial(os.Args[1], grpc.WithInsecure())
		if err != nil {
			log.Fatalf("Error while connecting to url: %v\n", err)
		}
		defer conn.Close()

		c := chatpb.NewChatClient(conn)
		stream, err := c.Chat(ctx)
		if err != nil {
			log.Fatalf("Error while creating chat client: %v\n", err)
		}

		var wg sync.WaitGroup
		wg.Add(2)

		go func() {
			for {
				msg, err := stream.Recv()
				if err == io.EOF {
					break
				} else if err != nil {
					log.Fatalf("Error while receiving from stream: %v\n", err)
				}
				fmt.Printf("%s: %s\n", msg.User, msg.Message)
			}
			wg.Done()
		}()

		fmt.Println("Connection stablished, type \"quit\" or use Ctrl+C to exit")
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			msg := scanner.Text()
			if msg == "quit" {
				err := stream.CloseSend()
				if err != nil {
					log.Fatalf("Error while closing stream: %v\n", err)
				}
				break
			}

			err := stream.Send(&chatpb.ChatMessage {
				User: os.Args[2],
				Message: msg,
			})
			if err != nil {
				log.Fatalf("Error while sending message: %v\n", err)
			}
		}

		wg.Wait()
}
