package main

import (
		"fmt"
		"io"
		"log"
		"google.golang.org/grpc"
		"github.com/icyd/go-grpc-lab/chat/chatpb"
		"net"
		"sync"
)

type Connection struct {
		conn chatpb.Chat_ChatServer
		send chan *chatpb.ChatMessage
		quit chan struct{}
}

func NewConnection(conn chatpb.Chat_ChatServer) *Connection {
		c := &Connection {
				conn: conn,
				send: make(chan *chatpb.ChatMessage),
				quit: make(chan struct{}),
		}
		go c.start()
		return c
}

func (c *Connection) Close() error {
		close(c.quit)
		close(c.send)
		return nil
}

func (c *Connection) Send(msg *chatpb.ChatMessage) {
		defer func() {
				recover()
		}()
		c.send <- msg
}

func (c *Connection) start() {
		for {
				select {
				case msg := <- c.send :
						c.conn.Send(msg)
				case <- c.quit:
						break
				}
		}
}

func (c *Connection) GetMessages(broadcast chan<- *chatpb.ChatMessage) error {
		for {
				msg, err := c.conn.Recv()
				if err == io.EOF {
						c.Close()
						return nil
				} else if err != nil {
						c.Close()
						return err
				}
				go func(msg *chatpb.ChatMessage) {
						select {
						case broadcast <- msg:
						case <- c.quit:
						}
				}(msg)
		}
}

type ChatServer struct {
		broadcast     chan *chatpb.ChatMessage
		quit          chan struct{}
		connections   []*Connection
		connLock      sync.Mutex
		chatpb.UnimplementedChatServer
}

func NewChatServer() *ChatServer {
		srv := &ChatServer {
			broadcast: make(chan *chatpb.ChatMessage),
			quit: make(chan struct{}),
		}
		go srv.start()
		return srv
}

func (c *ChatServer) Close() error {
		close(c.quit)
		return nil
}

func (c *ChatServer) start() {
		for {
			select {
			case msg := <- c.broadcast:
				c.connLock.Lock()
				for _, v := range c.connections {
					go v.Send(msg)
				}
				c.connLock.Unlock()
			case <- c.quit:
				break
			}
		}
}

func (c *ChatServer) Chat(stream chatpb.Chat_ChatServer) error {
		conn := NewConnection(stream)

		c.connLock.Lock()
		c.connections = append(c.connections, conn)
		c.connLock.Unlock()

		err := conn.GetMessages(c.broadcast)

		c.connLock.Lock()
		for i, v := range c.connections {
			if v == conn {
				c.connections = append(c.connections[:i], c.connections[i+1:]...)
			}
		}
		c.connLock.Unlock()

		return err
}

func main() {
		lst, err := net.Listen("tcp", ":50051")
		if err != nil {
			log.Fatalf("Error while creating server: %v\n", err)
		}

		s := grpc.NewServer()
		srv := NewChatServer()

		chatpb.RegisterChatServer(s, srv)

		fmt.Println("Now serving at port 50051")
		err = s.Serve(lst)
		if err != nil {
			log.Fatalf("Error when listening: %v\n", err)
		}

}

