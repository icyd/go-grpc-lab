package main

import (
	"context"
	"fmt"
	"log"
	"io"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/status"
	"github.com/icyd/go-grpc-lab/greet/greetpb"
	"sync"
	"time"
)
func main() {
	opts := grpc.WithInsecure()
	tls := true

	if tls {
		certFile := "ssl/ca.crt"
		creds, sslErr := credentials.NewClientTLSFromFile(certFile, "")
		if sslErr != nil {
			log.Fatalf("Failed to load certificates: %v\n", sslErr)
			return
		}

		opts = grpc.WithTransportCredentials(creds)
	}
	conn, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer conn.Close()

	c := greetpb.NewGreetServiceClient(conn)
	doUnary(c)
	// doServerStreaming(c)
	// doClientStreaming(c)
	// doBiDiStreaming(c)
	// doUnaryWithDeadline(c, 5*time.Second)
	// doUnaryWithDeadline(c, 1*time.Second)
}

func doUnary(c greetpb.GreetServiceClient) {
	req := &greetpb.GreetRequest {
		Greeting: &greetpb.Greeting {
			FirstName: "Alberto",
			LastName: "Vázquez",
		},
	}
	res, err := c.Greet(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling Greet RPC: %v", err)
	}
	log.Printf("Response from greet: %v", res.Result)
}

func doServerStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting server streaming RPC...")
	req := &greetpb.GreetManyTimesRequest {
		Greeting: &greetpb.Greeting {
			FirstName: "Alberto",
			LastName: "Vázquez",
		},
	}
	resStream, err := c.GreetManyTimes(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling GreetManyTimes RPC: %v", err)

	}

	for {
		msg, err := resStream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatalf("Error while reading stream: %v", err)
		} else {
			log.Printf("Response from GreetManyTimes: %v", msg.GetResult())
		}
	}
}

func doClientStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting client streaming RPC...")

	requests := []*greetpb.LongGreetRequest {
		&greetpb.LongGreetRequest {
			Greeting: &greetpb.Greeting {
				FirstName: "John",
			},
		},
		&greetpb.LongGreetRequest {
			Greeting: &greetpb.Greeting {
				FirstName: "Mary",
			},
		},
		&greetpb.LongGreetRequest {
			Greeting: &greetpb.Greeting {
				FirstName: "Lucy",
			},
		},
		&greetpb.LongGreetRequest {
			Greeting: &greetpb.Greeting {
				FirstName: "Mike",
			},
		},
	}
	stream, err := c.LongGreet(context.Background())
	if err != nil {
		log.Fatalf("Error while calling LongGreet RPC: %v", err)
	}

	for _, req := range requests {
		fmt.Printf("Sending req: %v\n", req)
		stream.Send(req)
		time.Sleep(1000 * time.Millisecond)
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error while receiving from LongGreet: %v", err)
	}

	fmt.Printf("LongGreet Response: %v\n", res)

}

func doBiDiStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting bidi streaming RPC...")

	requests := []*greetpb.GreetEveryoneRequest {
		&greetpb.GreetEveryoneRequest {
			Greeting: &greetpb.Greeting {
				FirstName: "John",
			},
		},
		&greetpb.GreetEveryoneRequest {
			Greeting: &greetpb.Greeting {
				FirstName: "Mary",
			},
		},
		&greetpb.GreetEveryoneRequest {
			Greeting: &greetpb.Greeting {
				FirstName: "Lucy",
			},
		},
		&greetpb.GreetEveryoneRequest {
			Greeting: &greetpb.Greeting {
				FirstName: "Mike",
			},
		},
	}

	stream, err := c.GreetEveryone(context.Background())
	if err != nil {
		log.Fatalf("Error while creating bidi stream: %v\n", err)
		return
	}

	var wg sync.WaitGroup

	wg.Add(2)

	go func() {
		for _, req := range requests {
			fmt.Printf("Sending message: %v\n", req)
			stream.Send(req)
			time.Sleep(1000 * time.Millisecond)
		}
		stream.CloseSend()
		wg.Done()

	}()

	go func() {
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				break
			} else if err != nil {
				log.Fatalf("Error while receiving data: %v\n", err)
				break
			}
			fmt.Printf("Received: %v\n", res.GetResult())
		}
		wg.Done()
	}()

	wg.Wait()
}

func doUnaryWithDeadline(c greetpb.GreetServiceClient, duration time.Duration) {
	fmt.Println("Starting unary with deadline RPC...")
	req := &greetpb.GreetWithDeadlineRequest {
		Greeting: &greetpb.Greeting {
			FirstName: "Alberto",
			LastName: "Vázquez",
		},
	}
	ctx, cancel := context.WithTimeout(context.Background(), duration)
	defer cancel()

	res, err := c.GreetWithDeadline(ctx, req)
	if err != nil {
		statusErr, ok := status.FromError(err)
		if ok {
			if statusErr.Code() == codes.DeadlineExceeded {
				fmt.Println("Timeout was exceeded")
			} else {
				fmt.Printf("Unexpeted error: %v\n", statusErr)

			}
		} else {
			log.Fatalf("Error while calling Greet RPC: %v", err)
		}
		return
	}
	log.Printf("Response from greet: %v", res.Result)
}
